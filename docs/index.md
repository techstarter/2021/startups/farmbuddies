# 1. Project Title: GearTracks

                                ### Image of Architecture Here

## Introduction 

<!-- Output copied to clipboard! -->

<!-----
NEW: Check the "Suppress top comment" option to remove this info from the output.

Conversion time: 14.387 seconds.


Using this Markdown file:

1. Paste this output into your source file.
2. See the notes and action items below regarding this conversion run.
3. Check the rendered output (headings, lists, code blocks, tables) for proper
   formatting and use a linkchecker before you publish this page.

Conversion notes:

* Docs to Markdown version 1.0β29
* Mon May 17 2021 18:01:10 GMT-0700 (PDT)
* Source doc: Farm Buddies
* This document has images: check for >>>>>  gd2md-html alert:  inline image link in generated source and store images to your server. NOTE: Images in exported zip file from Google Docs may not appear in  the same order as they do in your doc. Please check the images!

----->


<p style="color: red; font-weight: bold">>>>>>  gd2md-html alert:  ERRORs: 0; WARNINGs: 0; ALERTS: 24.</p>
<ul style="color: red; font-weight: bold"><li>See top comment block for details on ERRORs and WARNINGs. <li>In the converted Markdown or HTML, search for inline alerts that start with >>>>>  gd2md-html alert:  for specific instances that need correction.</ul>

<p style="color: red; font-weight: bold">Links to alert messages:</p><a href="#gdcalert1">alert1</a>
<a href="#gdcalert2">alert2</a>
<a href="#gdcalert3">alert3</a>
<a href="#gdcalert4">alert4</a>
<a href="#gdcalert5">alert5</a>
<a href="#gdcalert6">alert6</a>
<a href="#gdcalert7">alert7</a>
<a href="#gdcalert8">alert8</a>
<a href="#gdcalert9">alert9</a>
<a href="#gdcalert10">alert10</a>
<a href="#gdcalert11">alert11</a>
<a href="#gdcalert12">alert12</a>
<a href="#gdcalert13">alert13</a>
<a href="#gdcalert14">alert14</a>
<a href="#gdcalert15">alert15</a>
<a href="#gdcalert16">alert16</a>
<a href="#gdcalert17">alert17</a>
<a href="#gdcalert18">alert18</a>
<a href="#gdcalert19">alert19</a>
<a href="#gdcalert20">alert20</a>
<a href="#gdcalert21">alert21</a>
<a href="#gdcalert22">alert22</a>
<a href="#gdcalert23">alert23</a>
<a href="#gdcalert24">alert24</a>

<p style="color: red; font-weight: bold">>>>>> PLEASE check and correct alert issues and delete this message and the inline alerts.<hr></p>




<p id="gdcalert1" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image1.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert2">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image1.jpg "image_tooltip")


<p id="gdcalert2" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image2.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert3">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image2.jpg "image_tooltip")


**Farm Buddies **

**Individual**

**[https://forms.gle/MBShyxs4iVLNYNNz5](https://forms.gle/MBShyxs4iVLNYNNz5) **

**Team**

**[https://tinyurl.com/cgh2021teams](https://tinyurl.com/cgh2021teams)**

**Intro**

Farm buddies help automate your farm and helps increase your yields 

Farm buddies always come in a pair

One is responsible for sensing plant and growth conditions like temp humidity light intensity soil salt.

And the other buddy is the actuator buddy. Which controls automatic fertilizing switchin pumps, switching lights turning on fans etc.



<p id="gdcalert3" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image3.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert4">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image3.png "image_tooltip")


**How it works** 

So if the sensor buddy detects a condition that is not favorable for ur plants it can act on it for instance if soil is to dry it automatically messages the control buddy to switch on the irrigation pump

In cases of a hydroponics system if one buddy detects that the system needs more fertilizer it will notify buddy 2 to dose the system with fertilizer by switching the fertilizer pump on.

**Value Proposition**

Increase yield while reducing inputs 

Saves you time from managing compared to traditional farming

Affordable iot solution 

Monitor over mobile phone 

**Global Impact **

Farm buddies are small but they contribute to

LIST OF GLOBAL VALUES - 



*   Lowering greenhouse gas emissions and water pollution
*   Preventing depletion of soil and water reserves

manischa add here plz 

End with tagline : Farm buddies 

Parents to your plants


# 
    Contact information

Shayenne Overeem

[overeemshayenne@gmail.com](mailto:overeemshayenne@gmail.com)

+597 8684205

Shyfka landveld

[Shyfyemau@gmail.com](mailto:Shyfyemau@gmail.com)

+597 889-1716

Adjako Manischa

[amanischa@gmail.com](mailto:amanischa@gmail.com)

+597 7481227

Drishti boeddha

[Drishtiboeddha2@gmail.com](mailto:Drishtiboeddha2@gmail.com)

+597 8667383

Disha Boeddha

 [dishaboeddha@gmail.com](mailto:dishaboeddha@gmail.com)

+597 869-2004

**SCRIPT FOR ANIMATION VIDEO GOES HERE**

**Scene1**

Did u know it has been estimated that climate change may reduce global agriculture by 17% by 2050? And farmers are the most affected.

**Scene2**

As the earth warms up extreme flooding occurs depleting the global reservoir of water 

**Scene3**

 And extreme drought occurs causing the depletion of agrible lands.

**Scene4**

So we decided to build an Iot Solution 

**Scene5**


    Farm buddies always comes


    in a pair. you have body one which is a sensor node and body two the actuator node. these two bodies work together to automate your farm and increase your yields

**Scene 6**

**How does it work?** 

The sensor node would measure if soil is too dry and notify buddy 2 to irrigate the soil.

**Scene 7**

If the temperature increases over the maximum threshold, sensor node will notify his buddy to turn the fan on to cool the farm 

**Scene 8**

In case of an hydroponics system, if the system needs more fertilizer, the buddy 1 will sense and notify buddy 2 to dose the system with right amount of fertilizer **Scene 9**

And the best part of this all? You can control these sensors from just your mobile phone.

**Scene 10**

Farm buddies is very convenient ,affordable and has a user friendly dashboard.

**Scene 11 **

**##### **

 Video reference 

[https://www.youtube.com/watch?v=9Y0cayagnqQ](https://www.youtube.com/watch?v=9Y0cayagnqQ) Freepik and Illustrator

**Note: **

**Do not forget to add we can add more sensors more buddies**

**Pronunciation**

**Make sure audio volume is leveled**

**PHASE 2**

1. Do Research on how we can make our animation video better preferably without watermarks.



1. Blender for 2d and 3d animation 
2. (Flare?)  Rive 
3. Adobe After Effects
4. Adobe Character animator

2. Research on how we will have node 1 communicate with node 2.

They both work individually. ESP NOW

3. Integrate the live demo with the animation video.

We can rescan the video and see if we would like to iterate the script etc.

**Disha **this is where your project comes in.. let's schedule a day to come to the lab this week to do some work on what we have already.

**Shayenne Overeem** I propose you and @**Shyfka Landveld** do some research on the animation video making tools and how we can make our video better.

**EspNow :** [https://randomnerdtutorials.com/esp-now-two-way-communication-esp32/](https://randomnerdtutorials.com/esp-now-two-way-communication-esp32/)

[https://techtutorialsx.com/2019/10/20/esp32-getting-started-with-esp-now/](https://techtutorialsx.com/2019/10/20/esp32-getting-started-with-esp-now/) 


## **Step 2: About ESP-Now**



<p id="gdcalert4" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image4.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert5">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image4.jpg "image_tooltip")


• Communication protocol created by Espressif.

• You don’t need a WiFi network.

• Similar to the low-power protocol used on a 2.4GHz wireless mouse.

• Initial pairing required.

• After pairing, the connection is persistent peer-to-peer.

[https://www.instructables.com/ESP32-With-ESP-Now-Protocol/](https://www.instructables.com/ESP32-With-ESP-Now-Protocol/) 

165.47 meters in a straight line, using only the internal antennas of the devices 

libraries ESP_NOW.h and WiFi.h. 

To communicate via ESP-NOW, you need to know the MAC Address of the [ESP32 receiver](https://makeradvisor.com/tools/esp32-dev-board-wi-fi-bluetooth/). That’s how you know which device you’ll send the information to.

Each ESP32 has a [unique MAC Address](https://randomnerdtutorials.com/get-change-esp32-esp8266-mac-address-arduino/) and that’s how we identify each board to send data to it using ESP-NOW (learn how to [Get and Change the ESP32 MAC Address](https://randomnerdtutorials.com/get-change-esp32-esp8266-mac-address-arduino/)).


```
#include "WiFi.h"
void setup(){
  Serial.begin(115200);
  WiFi.mode(WIFI_MODE_STA);
  Serial.println(WiFi.macAddress());
}
void loop(){
}
```


ESP32 MAC : 24:6F:28:9E:8A:C8

uint8_t broadcastAddress[] = {0x24, 0x6F, 0x28, 0x9E, 0x8A, 0xC8};

##Mac address for esp8266 


```
#include <ESP8266WiFi.h>

void setup(){
  Serial.begin(115200);
  Serial.println();
  Serial.print("ESP8266 Board MAC Address:  ");
  Serial.println(WiFi.macAddress());
}
void loop(){
}
```


ESP8266 Board MAC Address:  5C:CF:7F:37:75:D7

[https://learn.circuit.rocks/esp-now-the-fastest-esp8266-protocol](https://learn.circuit.rocks/esp-now-the-fastest-esp8266-protocol) 

[https://randomnerdtutorials.com/esp-now-esp32-arduino-ide/](https://randomnerdtutorials.com/esp-now-esp32-arduino-ide/) 

**Sender Code  and Receiver Code here for esp32 ttgohigrow **

**[https://drive.google.com/drive/folders/1T9RNpklFSZq6oGq-yz6ZNnZVBgS_QFOb?usp=sharing](https://drive.google.com/drive/folders/1T9RNpklFSZq6oGq-yz6ZNnZVBgS_QFOb?usp=sharing) **

**## ESP8266 Sender receiver code here **

**[https://randomnerdtutorials.com/esp-now-esp8266-nodemcu-arduino-ide/](https://randomnerdtutorials.com/esp-now-esp8266-nodemcu-arduino-ide/) **

**The only difference btween esp32 and esp8266 are these libs **

**esp32**


```
#include <esp_now.h>
#include <WiFi.h>
```


**esp8266**


```
#include <ESP8266WiFi.h>
#include <espnow.h>
```


_  **// Init ESP-NOW**_

_if (esp_now_init() != ESP_OK) {_

_  Serial.println("Error initializing ESP-NOW");_

_  return;_

_}_

**one ESP32 to multiple ESP32 or ESP8266 boards (one-to-many configuration)**



<p id="gdcalert5" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image5.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert6">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image5.jpg "image_tooltip")




*   **sending the same message to all boards;**
*   **sending a different message to each board.**

**[https://embedgyan.wordpress.com/2020/04/17/esp-now-with-esp32-send-data-to-multiple-boards-one-to-many/](https://embedgyan.wordpress.com/2020/04/17/esp-now-with-esp32-send-data-to-multiple-boards-one-to-many/) **

**CONCLUDED :**

**Tested TTGOHIGROW ESP32 SENDING DATA TO BOTH ESP32 AND ESP8266 OVER PEER TO PEER NETWORK  One to Many **

**Code here [https://drive.google.com/drive/folders/1T9RNpklFSZq6oGq-yz6ZNnZVBgS_QFOb?usp=sharing](https://drive.google.com/drive/folders/1T9RNpklFSZq6oGq-yz6ZNnZVBgS_QFOb?usp=sharing) **

**Todo **

**Two way communication **

**[https://randomnerdtutorials.com/esp-now-esp32-arduino-ide/](https://randomnerdtutorials.com/esp-now-esp32-arduino-ide/) **

**Todo : espnow esp32 webserver**

**[https://randomnerdtutorials.com/esp32-esp-now-wi-fi-web-server/](https://randomnerdtutorials.com/esp32-esp-now-wi-fi-web-server/) **

**2 way**

**[https://randomnerdtutorials.com/esp-now-two-way-communication-esp8266-nodemcu/](https://randomnerdtutorials.com/esp-now-two-way-communication-esp8266-nodemcu/) **

**April 2 2021 **

TTghigrow wake up button 35 

#define USER_BUTTON         35 



<p id="gdcalert6" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image6.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert7">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image6.png "image_tooltip")


TO KNOW

Pin 4 Has to be set to High on Boot for the Sensors to be turned on!

I2C Ports have to be manually changed for the BH1750 Light sensor to be recognized.

Analog Soil Moisture sensor works fine but i never managed to get any readings on the Salt Sensor (GPIO 34)

The BatteryVoltage according to liligy Github code is off by about 0.1V .... gonna try more with a decent power supply that allows for specific voltage when it arrives.

#define POWER_CTRL          4

digitalWrite(POWER_CTRL, 1); // Turn power on dht

  delay(1000);

digitalWrite(POWER_CTRL, 0); // Turn power off 

Done : [https://github.com/ponys/espNow](https://github.com/ponys/espNow) ESP32 DHT11 Master Slave 

Master to slave communication tested and working.



<p id="gdcalert7" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image7.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert8">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image7.png "image_tooltip")




<p id="gdcalert8" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image8.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert9">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image8.png "image_tooltip")




<p id="gdcalert9" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image9.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert10">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image9.jpg "image_tooltip")


<p id="gdcalert10" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image10.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert11">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image10.jpg "image_tooltip")


<p id="gdcalert11" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image11.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert12">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image11.jpg "image_tooltip")


Done : Multiple TTG HIGROW sensor node communicating to one dash...

Todo: 

Test esp32 master code on the esp8266

Control Pump based on sensor data.

Show sensor reading on dash. 

Disha todo here: 

Today covered links and notes:

[https://randomnerdtutorials.com/esp-now-two-way-communication-esp8266-nodemcu/](https://randomnerdtutorials.com/esp-now-two-way-communication-esp8266-nodemcu/)

[https://techtutorialsx.com/2019/10/20/esp32-getting-started-with-esp-now/#The_sender_code](https://techtutorialsx.com/2019/10/20/esp32-getting-started-with-esp-now/#The_sender_code)

[https://www.instructables.com/ESP32-With-ESP-Now-Protocol/](https://www.instructables.com/ESP32-With-ESP-Now-Protocol/)

Today I started off with adding the code (measures temp humidity etc) to the sender and receiver node. ([code](https://docs.google.com/document/d/1GQUf5goEKejIzEic4zxLkI6jZxEJYPSQ9T-0WdmkqJE/edit?usp=sharing) here). To see the results of the sensors, make sure you have the node running, connect to it on the internet and refresh the page. (The IP we use is 192.168.4.1).

After that I worked on the DHT 11 sensor pinout.



<p id="gdcalert12" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image12.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert13">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image12.png "image_tooltip")


I connected on the NodeMCU ground to ground, voltage to 3V, and signal to D4.

Then I went to Arduino IDE and added the code to see if the DHT sensor was working. ([code](https://docs.google.com/document/d/1GQUf5goEKejIzEic4zxLkI6jZxEJYPSQ9T-0WdmkqJE/edit?usp=sharing))

**Enclosure.**

For the enclosure logo I'm using the FarmBuddies logo. I put it into inkscape. I then click Path - trace bitmap. Have live preview on and make your logo dark. After this, export as SVG and save it. Import it in Tinkercad.

 

<p id="gdcalert13" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image13.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert14">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image13.jpg "image_tooltip")




<p id="gdcalert14" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image14.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert15">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image14.jpg "image_tooltip")




<p id="gdcalert15" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image15.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert16">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image15.png "image_tooltip")


Download Enclosure integrate farm buddy logo to enclosure cover 

Ready for 3d printing [https://github.com/Xinyuan-LilyGO/TTGO-HiGrow](https://github.com/Xinyuan-LilyGO/TTGO-HiGrow) 

## Stl for esp8266 actuator node here but must be scaled to fit relay block

[https://www.thingiverse.com/thing:3380463](https://www.thingiverse.com/thing:3380463) and version 0.1

###3D Printed 



<p id="gdcalert16" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image16.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert17">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image16.jpg "image_tooltip")


<p id="gdcalert17" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image17.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert18">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image17.jpg "image_tooltip")


<p id="gdcalert18" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image18.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert19">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image18.jpg "image_tooltip")


**Script here **

**Shyfka Draft **

Story farm buddies 

Conversation between two plants Mr tomato and Mr carrot

Mr tomato:  Have you been experiencing the same thing i have been experiencing for the past years?

Mr Carrot: What are you talking about Mr Tomato? Because a lot has been taking place lately

Mr Tomato: Okayy i will explain, The ground i am planted on is getting dryer by day and unhealthier. I cant  even grow to my full potential

Mr Carrot: oooooo, i thought i was the only going through this. But I think it's called climate change.  Do You know the worst of it?

I heard Mr Farm(the farm owner) saying that climate change may reduce …..by 2050 by 17%

Mr tomato: Ooooh that is really bad. Who will save us from dying. How will we feed the world?

Sad music

And then a screen appears with farm buddies and all the cool stuff it does

after introducing farm buddies to Mr Farm, mr carrot and mr tomato are happy and producing healthy vegetables

Other script!

Ms farmer is seeing her plants all sad and dehydrated. She then goes on and does research as to why her plants are sad and then she finds out about how climate change is estimated to reduce agriculture by 17% by 2050 and all the other bad things. She then sees an ad for “Farm buddies”. She buys farm buddies.*insert live demo* And now her plants are happy and she is also happy.End with *Farm buddies,parents to your plants*

**Closing**

**## Manischa  This script was chosen **

**Adjuma Peppa - Pepper**

**And Pumpykin - Pumpkin**

A conversation between a plant from a Traditional farm and a plant from an Automated farm.

Plant A: How are you, Tommy?

Plant T: I'm fine, Finn

Plant A: You don't look fine to me. Your leaves are sloppy and your stem looks fragile. What has the farmer been doing to you?

Plant T: Well... I don't think he knows how much nutrients I need. But he's a good guy though.

Plant A: My farmer has Farm Buddies to take care of us. They're like parents to us and they know what we need.

Plant T: Wow. That sounds amazing. I can see from your leaves that you're well taken care of.

Plant A: Maybe your farmer could use Farm Buddies too. It's convenient and the farmer can give you everything you need by checking your status on the dashboard.

2 weeks later

Plant A: Timmy is that you ?

Plant T: Yes. I'm so glad I took your advice.

Plant A: You are most certainly welcome. Did you notice how the Buddies work?

Plant T: Uhuh. They communicate with each other to give you the things you lack.

Plant A: I heard from my farmer that Farm Buddies can lower greenhouse gas emissions and prevent the depletion of soil.

Plant T: i think that's a fact.

 

**Disha Draft:**

Plant: Howdy neighbor! Are you also having a hard time getting enough nutrients?

Neighbour plant: No not at all! I heard that climate change will reduce over 17% by 2050...for that reason, my owner got me Farm Buddies! It keeps me supplied all day and at all times!

Plant: What is Farm Buddies? What does it do? How does it work?

Neighbour plant:

                        (Show the 2 nodes) Farm Buddies consists of 2 or more TT-Higrow nodes which measures my temperature humidity, light intensity, soil, salt, and water.

                        (Show a picture of healthy plants/fruits)  If I am lacking any of those I get it immediately and that's how I stay healthy!

Plant: That sounds wonderful...do you know how I could get it?

Neighbour plant: Ofcourse! 

                          (Show picture of business canvas)

**Closing**

**Shayenne draft:**

Ms Farmer sees her plants sad*

She proceeds to ask them what is wrong: “Can I ask what’s wrong?”

The plants then go on and tell her that the ground is dry and they can't grow to their full potential. She does some research and finds out that due to climate change ,agriculture is estimated to reduce by 17% by 2050. She then sees an ad with farm buddies and she then buys it.*insert live demo* The plants get happy and Ms farmer is also happy :)

 

##### Final Script 

Hello. My name is Disha Boeddha.  My teammates are Drishti Boeddha, Manischa Adjako, Shyfka Landveld and Shayenne Overeem and we are from Paramaribo, Suriname.

Today we are presenting Farm Buddies.

Farm buddies can help against the negative impact climate change has on farming., and we would love to share with you our animation video!

scene1

it has been computed that climate change may reduce global agriculture by 17% in 2050. this climate change will affect farmers the most

  

scene2

As the earth warms up, extreme flooding occurs, depleting the global reservoir of water

scene3

and extreme drought occurs, causing the depletion of agricultural lands.

scene4

So we decided to build an iot solution

scene5

how have our crops experienced farm buddies? Now let's find out!

scene6 

 A: How are you, Pumpykin?

 P: I'm fine, Adjuma

 A: You don't look fine to me. Your leaves are sloppy and your stem looks fragile. What has the farmer been doing to you?

 P: Well... I don't think he knows how much nutrients I need. But he's a good guy though

 A: My farmer has Farm Buddies to take care of us. They're like parents to us and they know what we need.

 P: Wow. That sounds amazing. I can see from your leaves that you're well taken care of.

 A: Maybe your farmer could use Farm Buddies too. It's convenient and the farmer can give you everything you need by checking your status on the dashboard

 P: How does Farm Buddies work?

 

scene7

Farm buddies always comes in a pair.

You have body one, which is a sensor node, and body two, the actuator node. These two bodies work together to automate your farm and increase your yields

The sensor node would measure if soil is too dry and notify buddy actuator node to irrigate the soil.

If the temperature increases over the maximum threshold, sensor node will notify his buddy actuator node to turn the fan on to cool the farm 

When the hydroponics system requires  more fertilizer, buddy sensor node will sense and notify buddy actuator node to dose the system with right amount of fertilizer

And the best part of this all? You can control these sensors from just your mobile phone.

Farm buddies is very convenient ,affordable and has a user friendly dashboard.

scene8

2 weeks later

A: Pumpykin is that you ?

P: Yes. I'm so glad I took your advice.

A: You are most certainly welcome. Did you notice how the Buddies work?

P: Uhuh. They communicate with each other to give you the things you lack.

A: I heard from my farmer that Farm Buddies can lower greenhouse gas emissions and prevent the depletion of soil.

P: i think that's a fact.

**ANIMATION SECTION HERE**

**[https://drive.google.com/file/d/1gn1ldYFz41uGEbqDMH5Ser_CbHawPfl4/view?usp=sharing](https://drive.google.com/file/d/1gn1ldYFz41uGEbqDMH5Ser_CbHawPfl4/view?usp=sharing)  first draft farm buddies video**

**Tools: **

**Remove gif background [https://www.unscreen.com/upload](https://www.unscreen.com/upload) **

**Mp4 to mp3 converter [https://cloudconvert.com/mp4-to-mp3](https://cloudconvert.com/mp4-to-mp3) **

**Get script [https://youtu.be/pOLAIVUs9S8](https://youtu.be/pOLAIVUs9S8) **

**Remove background from image [https://www.remove.bg/upload](https://www.remove.bg/upload) **

**Compress gif [https://www.iloveimg.com/compress-image/compress-gif](https://www.iloveimg.com/compress-image/compress-gif) **

**Trim gif [https://ezgif.com/cut](https://ezgif.com/cut) **

**Download youtube video [https://yt1s.com/en5](https://yt1s.com/en5) **

**How will we do the demo?**

**Rewrite script voice over**

**Dashboard need color scheme how does it look on mobile **

**Enclosure need printing sensor and actuator node**

**Photoshoot**


![alt_text](images/image24.jpg "image_tooltip")


Jobs Done

Name : Task Completed : Time \
 \
Drishti: Script adjusting ; looking for background music ; talking for animation : 2-3hours? (not quite sure)

Next steps 

Test the kpi system for stocks theo Boomsma course 

Business canvas / market research

Building of actuator node with switch 

Ideas to water tight to field test 

Working on the communication protocol espnow


## Demo / Proof of work


## Deliverables

< The deliverables of the project, so we know when it is complete>
- Project poster
- Proof of concept working
- Product pitch deck


## Project landing page/website (optional)


## the team & contact



# Contact


